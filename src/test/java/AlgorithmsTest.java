
import com.rexlabs.pi.PiAlgorithms;
import org.apfloat.Apfloat;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigInteger;
import java.util.HashMap;

/**
 * Testing algorithms by giving right pi number
 */
public class AlgorithmsTest {

    static HashMap<BigInteger, Apfloat> piNumbers = new HashMap<>();
    static {
        piNumbers.put(BigInteger.valueOf(35L), new Apfloat("3.14159265358979323846264338327950288"));
        piNumbers.put(BigInteger.valueOf(100L), new Apfloat("3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679"));
    }

    @Test
    public void testAlgorithms() {
        PiAlgorithms.forEachAlgorithm(
                (name, algo) -> piNumbers.forEach(
                        (countOfDigits, value) -> {
                            Apfloat computed = algo.computePi(countOfDigits);
                            if (!computed.equals(value)) {
                                Assert.fail("Algorithm "
                                        + name + " computed wrong number at stage with expected "
                                        + countOfDigits.toString() + " after dot || "
                                        + "Required -> " + value + " || "
                                        + "Computed -> " + computed + ""
                                );
                            }
                        }
                )
        );
    }

}
