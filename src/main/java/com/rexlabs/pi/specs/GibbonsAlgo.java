package com.rexlabs.pi.specs;

import com.rexlabs.pi.PiAlgorithm;
import org.apfloat.Apfloat;
import org.apfloat.Apint;


import java.math.BigInteger;

/**
 * Implements single-thread Spigot Algorithm {@link "http://www.cs.ox.ac.uk/people/jeremy.gibbons/publications/spigot.pdf"}
 */
public class GibbonsAlgo implements PiAlgorithm {

    static final Apint TWO = new Apint(BigInteger.valueOf(2L));
    static final Apint THREE = new Apint(BigInteger.valueOf(3L));
    static final Apint FOUR = new Apint(BigInteger.valueOf(4L));
    static final Apint SEVEN = new Apint(BigInteger.valueOf(7L));

    @Override
    public Apfloat computePi(BigInteger limit) {

        Apint q = Apint.ONE;
        Apint r = Apint.ZERO;
        Apint t = Apint.ONE;
        Apint k = Apint.ONE;
        Apint n = new Apint(BigInteger.valueOf(3L));
        Apint l = new Apint(BigInteger.valueOf(3L));

        BigInteger limit1 = limit.add(BigInteger.ONE);
        Apint nn, nr;
        boolean first = true;
        BigInteger counter = BigInteger.ONE;
        StringBuilder sb = new StringBuilder();
        while (counter.compareTo(limit1) <= 0) {
            if (FOUR.multiply(q).add(r).subtract(t).compareTo(n.multiply(t)) == -1) {
                sb.append(n) ;
                if (first) {
                    sb.append(".");
                    first = false;
                }
                nr = new Apint(BigInteger.TEN).multiply(r.subtract(n.multiply(t)));
                n = new Apint(BigInteger.TEN)
                        .multiply(THREE
                                .multiply(q)
                                .add(r))
                        .divide(t)
                        .subtract(new Apint(BigInteger.TEN).multiply(n));
                q = q.multiply(new Apint(BigInteger.TEN));
                r = nr;
                counter = counter.add(BigInteger.ONE);
            } else {
                nr = TWO.multiply(q).add(r).multiply(l) ;
                nn = q.multiply((SEVEN.multiply(k))).add(TWO).add(r.multiply(l)).divide(t.multiply(l)) ;
                q = q.multiply(k) ;
                t = t.multiply(l) ;
                l = l.add(TWO) ;
                k = k.add(Apint.ONE) ;
                n = nn ;
                r = nr ;
            }
        }
        return new Apfloat(sb.toString());

    }


}
