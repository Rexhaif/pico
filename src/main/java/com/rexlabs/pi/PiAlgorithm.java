package com.rexlabs.pi;

import org.apfloat.Apfloat;

import java.math.BigInteger;

/**
 * Describes methods to compute limited number of pi digits
 */
public interface PiAlgorithm {
    /**
     * Compute pi digits by given limit
     * @param limit count of digits AFTER DOT(not include first digit -> 3)
     * @return computed pi number
     */
    Apfloat computePi(BigInteger limit);

}
