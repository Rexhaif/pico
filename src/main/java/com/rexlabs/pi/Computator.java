package com.rexlabs.pi;

import com.rexlabs.pi.utils.TimedOperation;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * Default algorithm manual testing utility
 */
public class Computator {

    public static void main(String[] args) {
        System.out.println("The Pi computation engine");
        System.out.println("Available algorithms -> ");
        PiAlgorithms.algoNames().forEach(v -> System.out.println("    " + v));
        Scanner in = new Scanner(System.in);
        System.out.println("Enter number of digits to computate it using different available algorithms");
        System.out.print("/-> ");
        BigInteger limit = BigInteger.valueOf(in.nextLong());
        System.out.println("Benchmarking for " + limit.toString() + " digits after dot -> ");
        PiAlgorithms.forEachAlgorithm((k, v) -> {
            TimedOperation computation = new TimedOperation(
                    () -> System.out.println(
                            v.computePi(limit)
                                    .toString(true)
                    )
            );
            System.out.println(k + " Algorithm -> ");
            System.out.print("    Computation Result -> ");
            double seconds = computation.run();
            System.out.println("    Total time -> " + seconds + " seconds");
        });
        System.out.println("Type anything and press Enter to exit...");
        in.next();
    }

}
