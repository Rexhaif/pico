package com.rexlabs.pi.utils;

/**
 * Represents lite benchmarking
 */
public class TimedOperation {

    private Runnable operation;

    /**
     * Construct instance with specified {@link Runnable}
     * @param operation Runnable, that need benchmarking
     */
    public TimedOperation(Runnable operation) {
        this.operation = operation;
    }

    /**
     * Executes operation and retrieves how much time it take
     * @return time of execution, in seconds
     */
    public double run() {
        long t1 = System.nanoTime();
        operation.run();
        long diff = System.nanoTime() - t1;
        return (double) diff / 1000000000.0D;
    }

}
