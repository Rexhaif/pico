package com.rexlabs.pi;

import com.rexlabs.pi.specs.ChudnovskyAlgo;
import com.rexlabs.pi.specs.GibbonsAlgo;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiConsumer;

/**
 * Factory of pi computation algorithms
 */
public class PiAlgorithms {

    private static HashMap<String, PiAlgorithm> algorithms = new HashMap<>(15);

    static {
        registerAlgorithm("GIBBONS/SINGLE_THREAD", new GibbonsAlgo());
        registerAlgorithm("CHUDNOVSKY/SINGLE_THREAD", new ChudnovskyAlgo.SingleThread());
        registerAlgorithm("CHUDNOVSKY/MULTI_THREAD", new ChudnovskyAlgo.MultiThread());
    }

    /**
     * Retrieving {@link PiAlgorithm} by specified name
     * @param name name of algorithm
     * @return existed algorithm with specified name
     * @throws NoSuchAlgorithmException if algorithm is not exist
     */
    public static PiAlgorithm forName(String name) throws NoSuchAlgorithmException{
        if (!algorithms.containsKey(name)) {
            throw new NoSuchAlgorithmException("Algorithm " + name + " does not exist");
        } else {
            return algorithms.get(name);
        }
    }

    /**
     * Dynamically add the algorithm to the register
     * @param name name of algorithm. Use this pattern -> "[name of algorithm]/[computation method (SINGLE_THREAD, MULTI_THREAD, GPU etc.)"
     *             for example -> name "CHUDNOVSKY/MULTI_THREAD"
     *             specifies Chudnovsky algorithm implementation, that uses multiple threads in computation
     * @param algo instance of algorithm
     */
    public static void registerAlgorithm(String name, PiAlgorithm algo) {
        algorithms.put(name, algo);
    }

    /**
     * Iterates over available algorithms, can be used for benchmarks
     * @param consumer callback, that will called at each algorithm in register
     */
    public static void forEachAlgorithm(BiConsumer<String, PiAlgorithm> consumer) {
        algorithms.forEach(consumer);
    }

    /**
     * Retrieves List of names of available algorithms in register
     * @return {@link List<String>} that contains names of existed algorithms
     */
    public static List<String> algoNames() {
        return new ArrayList<>(algorithms.keySet());
    }

}
