**# The Pi computation engine #**

## Usage -> ##

```
#!java

PiAlgorithm algo = PiAlgorithms.forName("Preferred Algorithm");
Apfloat pi = algo.computePi(BigInteger.valueOf(100L));
System.out.println(pi.toString(true));
```
Available by default algorithms:

1. Gibbons algorithm (single thread impl from Rosettacode [link](http://rosettacode.org/wiki/Pi#Java)) -> 
call function forName() using following parameter - "GIBBONS/SINGLE_THREAD"
2. Chudnovsky algorithm (single and multithread impl from [girhub](https://github.com/lemmingapex/ChudnovskyAlgorithm)) -> 
call function forName() using following prameters - "CHUDNOVSKY/SINGLE_THREAD" or "CHUDNOVSKY/MULTI_THREAD"


Also you can get all available algorithm names using: 
```
#!java

List<String> algoNames = PiAlgorithms.algoNames();
```
And iterate over available algorithms:

```
#!java

PiAlgorithms.forEachAlgorithm(
        (nameOfAlgo, algo) -> {
            System.out.println("Algorithm " 
                    + nameOfAlgo + " computed number -> " + algo.computePi(BigInteger.valueOf(100L)).toString(true)
            );
        }
)
```

Add your custom algorithm in runtime:

```
#!java

PiAlgorithms.registerAlgorithm("MYALGO/SINGLE_THREAD", new MyAwesomeAlgorithm());
```
Or, better, clone repo, add your algorithm to Pico, and create pull request.
You can add algorithm in static section of PiAlgorithms class:

```
#!java

static {
        registerAlgorithm("GIBBONS/SINGLE_THREAD", new GibbonsAlgo());
        registerAlgorithm("CHUDNOVSKY/SINGLE_THREAD", new ChudnovskyAlgo.SingleThread());
        registerAlgorithm("CHUDNOVSKY/MULTI_THREAD", new ChudnovskyAlgo.MultiThread());
        ...
        registerAlgorithm("MYALGO/SINGLE_THREAD", new MyAwesomeAlgorithm());
    }
```
Also you can run mvn -B test to check that you implementation compute right numbers.